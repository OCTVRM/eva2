package com.example.root.eval;

import android.net.Uri;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity implements FragmentoUno.OnFragmentInteractionListener,FragmentoDos.OnFragmentInteractionListener, FragmentoTres.OnFragmentInteractionListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentManager fragmentManager0 = getSupportFragmentManager();
        FragmentTransaction transition1 = fragmentManager0.beginTransaction();
        FragmentoUno fragmentoUno = new FragmentoUno();
        transition1.replace(R.id.flContenedor,fragmentoUno);
        transition1.commit();

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
